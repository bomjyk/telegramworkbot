using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Library;

namespace Consumer
{
    public class Startup
    {
        ConnectionMultiplexer Server = ConnectionMultiplexer.Connect(Constants.RedisConnection);
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Library.IRedis, RedisRealese1>();
        }
        public void Configure(IApplicationBuilder app)
        {
            RedisRealese1 realese = new RedisRealese1();
            RedisClient redis = new RedisClient(realese);
            Console.WriteLine(redis.GetString("sasha"));
            Console.ReadKey();
        }
    }
}