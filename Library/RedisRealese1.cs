using StackExchange.Redis;

namespace Library
{
    public class RedisRealese1 : IRedis
    {
        public IDatabase database { get {return _database;} }
        public ConnectionMultiplexer server {get{return _server;} }
        public RedisRealese1()
        {
            _server = ConnectionMultiplexer.Connect(Constants.RedisConnection);
            _database = _server.GetDatabase();
        }
        IDatabase _database;
        ConnectionMultiplexer _server;
        public void SetNewDb(IDatabase newDatabase)
        {
            _database = newDatabase;
        }
        public void SetString(string key, string text) {
            _database.StringSet(key,text);
        }
        public string GetString(string key) {
            return _database.StringGet(key);
        }
    }
}