using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using StackExchange.Redis;

namespace Library
{
    public class RedisClient
    {
        readonly IRedis redis;
        
        public RedisClient (IRedis redis)
        {
            this.redis = redis;
        }
        public void SetNewDb(IDatabase newDatabase)
        {
            this.redis.SetNewDb(newDatabase);
        }
        public void SetString(string key, string text) {
            this.redis.SetString(key,text);
        }
        public string GetString(string key) {
            return this.redis.GetString(key);
        }
    }
}