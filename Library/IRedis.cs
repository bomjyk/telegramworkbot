using StackExchange.Redis;

namespace Library
{
    public interface IRedis
    {
        ConnectionMultiplexer server {get;}
        IDatabase database {get;}
        void SetString(string key, string text);
        void SetNewDb(IDatabase database);
        string GetString(string key);
    }
}